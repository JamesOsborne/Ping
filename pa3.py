from socket import *
import os
import struct
import time
import select

ICMP_ECHO_REQUEST = 8 # Fill in...
IP_ICMP_PROTOCOL  = 1 # the correct vals...
ICMP_ECHO_REPLY   = 0 # for these...
ICMP_ECHO_CODE    = 0 # constants

def checksum(str):
    csum = 0
    countTo = (len(str) // 2) * 2 # floor division
    count = 0
    while count < countTo:
        thisVal = str[count+1] * 256 + str[count]
        csum = csum + thisVal
        csum = csum & 0xffffffff
        count = count + 2

    if countTo < len(str): # catches the last byte for odd length str
        csum = csum + str[len(str) - 1]
        csum = csum & 0xffffffff

    csum = (csum >> 16) + (csum & 0xffff)
    csum = csum + (csum >> 16)
    answer = ~csum
    answer = answer & 0xffff
    answer = answer >> 8 | (answer << 8 & 0xff00)
    return answer

def receiveOnePing(mySocket, ID, timeout, destAddr, seqNum):
    timeLeft = timeout
    while 1:
        startedSelect = time.time()
        whatReady = select.select([mySocket], [], [], timeLeft)
        howLongInSelect = (time.time() - startedSelect)
        if whatReady[0] == []: # Timeout
            return "Request timed out."

        timeReceived = time.time()
        recPacket, addr = mySocket.recvfrom(1024)

        print("[*] Received packet of length:", len(recPacket))

        # ********************************************
        #Fetch the IP header data you need
        sourceIP = f'IP: {recPacket[12]}.{recPacket[13]}.{recPacket[14]}.{recPacket[15]}'
        destinationIP = f'{recPacket[16]}.{recPacket[17]}.{recPacket[18]}.{recPacket[19]}'
        ttl = recPacket[8]
        isUpperLayerICMP = recPacket[9] == IP_ICMP_PROTOCOL

        #Print IP data
        print(f'IP: {sourceIP} -> {destinationIP}')
        print(f'TTL: {ttl}')
        print(f'Upper Layer ICMP? {isUpperLayerICMP}')

        #Fetch the ICMP header data you need
        icmpPacket = recPacket[20:]

        #Header will have type and code zero,
        #and must zero out checksum for calculation
        icmpHeader = b'\x00\x00\x00\x00' + icmpPacket[4:8]
        icmpData = icmpPacket[8:]
        icmpCalculatedChecksum = htons(checksum(icmpHeader + icmpData))
        #After calculating checksum, get actual header
        icmpHeader = icmpPacket[0:8]

        icmpType, icmpCode, icmpChecksum, dataID, dataSeqNum = struct.unpack(
            'bbHHh', 
            icmpHeader)
        data = struct.unpack('d', icmpData)[0]

        isChecksumValid = icmpCalculatedChecksum == icmpChecksum
        isICMPEchoCode = icmpType == ICMP_ECHO_CODE
        idAndDataIdNumMatch = dataID == ID

        #Print ICMP data
        print(f'ICMP Data: {data}')
        print(f'ICMP Checksum Valid? {isChecksumValid}')
        print(f'ICMP Echo Reply? {isICMPEchoCode}')
        print(f'ICMP ID & Seq Num match? {idAndDataIdNumMatch}')

        #Return total time elapsed in ms
        return (time.time() - startedSelect) * 1000
        # ********************************************

        timeLeft = timeLeft - howLongInSelectf
        if timeLeft <= 0:
            return "Request timed out."

def sendOnePing(mySocket, destAddr, ID, seqNum):
    # Header is type (8), code (8), checksum (16), id (16), sequence (16)

    myChecksum = 0
    # Make a dummy header with a 0 checksum.
    # struct -- Interpret strings as packed binary data
    header = struct.pack("bbHHh", ICMP_ECHO_REQUEST, 0, myChecksum, ID, seqNum)
    data = struct.pack("d", time.time())
    # Calculate the checksum on the data and the dummy header.
    myChecksum = checksum(header + data)
    myChecksum = htons(myChecksum)

    header = struct.pack("bbHHh", ICMP_ECHO_REQUEST, 0, myChecksum, ID, seqNum)
    packet = header + data
    mySocket.sendto(packet, (destAddr, 1))

def doOnePing(destAddr, timeout, seqNum):
    icmp = getprotobyname("icmp")

    mySocket = socket(AF_INET, SOCK_RAW, icmp)

    myID = os.getpid() & 0xFFFF #Returns the current process id
    sendOnePing(mySocket, destAddr, myID, seqNum)
    print('\nReceive ping')
    delay = receiveOnePing(mySocket, myID, timeout, destAddr, seqNum)

    mySocket.close()
    return delay

def ping(host, timeout=1):
    #timeout=1 means: If one second goes by without a reply from the server,
    #the client assumes that either the client’s ping or the server’s pong is lost
    dest = gethostbyname(host)
    print("Pinging " + dest + " using Python")
    print("")
    #Send ping requests to a server separated by approximately one second
    for i in range(1,5) :
        print("[*] Sending ping", i, "...")
        delay = doOnePing(dest, timeout, i)
        print("[*] Delay:", delay, "ms")
        print() # printing blank line
        time.sleep(1)# one second
    print("Done Pinging " + dest)


ping("www.google.com")
